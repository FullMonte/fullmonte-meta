# FullMonte-Meta

A META project used to organize all sub-projects in the FullMonte group. The FullMonte suite contains several projects related to interstitial PDT planning and light propagation in biological tissues. This wiki describes how all the tools developed in our suite fits together. 

## The FullMonte Suite of Tools consists of

* [MeshTool](https://gitlab.com/FullMonte/MeshTool):
    * Meshtool generates tetrahedral volume meshes of biological tissues from polyhedral surfaces such as those emitted by ITK-Snap. ITK-Snap is often used to delineate or segment tissues in 3D medical images; MeshTool can convert these segmented images into meshes that FullMonteSW (the light simulator) can operate upon.
<br/>

* [FullMonteSW](https://gitlab.com/FullMonte/FullMonteSW):
    * FullMonteSW is a fast and accurate simulator of light propagation which supports arbitrary media (tissue types) and geometries (3D structures). FullMonteSW also has accelerated versions on other compute platforms such as Graphics Urocessing Units (GPUs) using [CUDA with C++](https://gitlab.com/FullMonte/cudaaccel), and Field Programmable Gate Arrays (FPGAs) using [Bluespec](https://gitlab.com/FullMonte/FullMonteHW) and [OpenCL](https://gitlab.com/FullMonte/fpgaclaccel).
<br/>
  
* [PDT-SPACE](https://gitlab.com/FullMonte/pdt-space):
    * PDT-SPACE is a tool used for treatment planning of photodynamic therapy (PDT). It optimizes the power allocation and placement of a set of light probes to destroy a tumor while minimizing damage to the healthy organs-at-risk tissue surrounding the tumor. PDT-SPACE uses FullMonteSW to simulate the light propagation for the various probe power allocations and placements it considers. Additionally, PDT-SPACE acounts for the statistical variation in the tissue absorption and scattering coefficients (that govern how the light propagates) to increase robustness of the treatment plan.
<br/>

* [PDT GUI](https://gitlab.com/FullMonte/PDT-fullmonte-gui#pdt-fullmonte-gui):
    * Under construction: The PDT GUI will integrate the above tools in a single user interface, along with visualization of their outputs.
<br/>

## Process flow
  
```mermaid
flowchart LR
    A[MeshTool] --> B[Mesh]
    B1[Tissue's<br>Optical<br>Properties] --> C
    B --> C[FullMonteSW]
    B2[Light<br>Source<br>Positions] --> C
    C --> D[VTK File]
    D --> D1[VTK]
    subgraph VTK
    D1--> D2[3D Visualization<br>of Meshes]
    end
    C --> E[PDT Space]
    E --> C
    E --> F1[Optimized<br>Power of Sources]
    E --> F2[Optimized<br>Source<br>perturbation]
    E --> F3[Final<br>Fluence<br>Distriubtion]
```

## Installation
All tools in the FullMonte suite use [Docker containers](https://www.docker.com/) to provide either images with the tools precompiled for the users or images with the necessary build environments for developers. You can follow the following [snippet](https://gitlab.com/FullMonte/fullmonte-meta/snippets/1983016) to install the docker images.

## How to use
Each tool has a getting start guild on how to use. Please visit their respective repositories of the different tools for instructions on how to use each one.  
- [MeshTool](https://gitlab.com/FullMonte/MeshTool/-/wikis/home)
- [FullMonteSW](https://gitlab.com/FullMonte/FullMonteSW/-/wikis/home)
- [PDT-SPACE](https://gitlab.com/FullMonte/pdt-space/-/wikis/home)

## Directory structure
The list below summarizes the files directories in this directory:
*  `.gitlab-ci.yml`: The file that describes the CI/CD operations to execute
*  `README.md`: This file
*  `Docker-dev/`: All Docker related data for creating the development Docker image
*  `Docker-user/`: All Docker related data for creating the user Docker image

This CI/CD for this project has a [shedule](https://gitlab.com/FullMonte/fullmonte-meta/pipeline_schedules) set which runs the pipeline every Sunday at 0400 (Eastern Time). [This GitLab document](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html) describe in more detail how schedules work.

## Citation
If your work with any of the tools results in any academic publications, please give credit by citing:

* High-performance, robustly verified Monte Carlo simulation with FullMonte. https://spie.org/publications/journal/10.1117/1.JBO.23.8.085001?SSO=1 By: Cassidy, Jeffrey; Nouri, Ali; Betz, Vaughn; et al. Journal of biomedical optics  Volume: 23   Issue: 8   Pages: 1-11   Published: 2018-Aug

**Bibtex:**
```bibtex
@article{doi: 10.1117/1.JBO.23.8.085001,
author = { Jeffrey  Cassidy,Ali  Nouri,Vaughn  Betz,Lothar  Lilge},
title = {High-performance, robustly verified Monte Carlo simulation with FullMonte},
journal = {Journal of Biomedical Optics},
volume = {23},
number = {},
pages = {23 - 23 - 11},
year = {2018},
doi = {10.1117/1.JBO.23.8.085001},
URL = {https://doi.org/10.1117/1.JBO.23.8.085001},
eprint = {}
}
```

* Automatic interstitial photodynamic therapy planning via convex optimization https://www.osapublishing.org/boe/fulltext.cfm?uri=boe-9-2-898&id=381237 By: Yassine, Abdul-Amir; Kingsford, William; Xu, Yiwen; et al. BIOMEDICAL OPTICS EXPRESS  Volume: 9   Issue: 2   Pages: 898-920   Published: FEB 1 2018

**Bibtex:**
```bibtex
@article{doi: 10.1117/1.JBO.23.8.085001,
author = { Yassine, A.-A., Kingsford, W., Xu, Y., Cassidy, J., Lilge, L., & Betz, V.},
title = {Automatic interstitial photodynamic therapy planning via convex optimization},
journal = {Biomedical Optics Express},
volume = {9},
number = {2},
pages = {989 - 920},
year = {2018},
doi = {10.1117/1.JBO.23.8.085001},
URL = {http://doi.org/10.1364/BOE.9.000898},
eprint = {}
}
```
