########################################################################################
## Copy files and dependencies from the MeshTool project
FROM registry.gitlab.com/fullmonte/fullmontesw/fullmonte-run:latest AS fullmonte
########################################################################################

########################################################################################
#### Build ITKsnap-3.8.0
FROM ubuntu:16.04 as itksnap-3.8.0

# Download VTK-7.1.1 source
RUN apt-get update -y && apt-get install -y wget tar
WORKDIR /itksnap
RUN wget https://sourceforge.net/projects/itk-snap/files/itk-snap/3.8.0/itksnap-3.8.0-20190612-Linux-x86_64-qt4.tar.gz
RUN tar -zxf itksnap-3.8.0-20190612-Linux-x86_64-qt4.tar.gz
RUN mv itksnap-3.8.0-20190612-Linux-gcc64-qt4 ITKsnap-3.8.0
########################################################################################

## Copy files and dependencies from the PDT-SPACE project 
FROM registry.gitlab.com/fullmonte/pdt-space/pdt-space-run:master AS pdt-space-run

########################################################################################
## Copy files and dependencies from the FullMonte project
FROM registry.gitlab.com/fullmonte/meshtool/meshtool-run:latest AS fullmonte-meta

RUN apt-get update && apt-get install -y libbz2-1.0 bzip2 zlib1g p7zip-full cpuid tcl rlwrap
RUN apt-get update -y && apt-get install -y libcgal11v5 libboost-all-dev libxt6 libx11-6 libqt5x11extras5
RUN apt-get update -y && apt-get install -y libeigen3-dev libssl-dev gfortran vim
RUN apt-get update -y && apt-get install -y libcurl3 libglu1-mesa libxi-dev libxmu-dev libglu1-mesa-dev
# Install Paraview
RUN apt-get update -y && apt-get install -y paraview


# We should not need these commands since we are basing the new image of of meshtool-run:latest
#COPY --from=meshtool            /usr/local/MeshTool             							/usr/local/MeshTool
#COPY --from=meshtool            /usr/local/Qt                   							/usr/local/Qt
#COPY --from=meshtool            /usr/local/VTK-7.1.1                   					/usr/local/VTK-7.1.1

COPY --from=itksnap-3.8.0 /itksnap/ITKsnap-3.8.0/bin            /usr/local/ITKsnap/bin
COPY --from=itksnap-3.8.0 /itksnap/ITKsnap-3.8.0/lib            /usr/local/ITKsnap/lib


# Copy the files we need from the fullmonte-run:latest image to our new image
COPY --from=fullmonte                /usr/local/FullMonteSW          /usr/local/FullMonteSW
COPY --from=fullmonte                /usr/local/boost-1.58.0         /usr/local/boost-1.58.0
# VTK install of MeshTool covers more features, that is why we will be using the Meshtool image as base image
#COPY --from=fullmonte                /usr/local/VTK-7.1.1            /usr/local/VTK-7.1.1

COPY --from=pdt-space-run		/usr/local/mosek-9.1.13										/usr/local/mosek-9.1.13
COPY --from=pdt-space-run		/usr/local/pdt-space										/usr/local/pdt-space
# Fynn: We should not need this anymore. I think MeshTool has a complete build of VTK included in its image
#COPY --from=pdt-space-run		/usr/local/VTK-7.1.1/lib/libvtkRenderingFreeType-7.1.so.1	/usr/local/VTK-7.1.1/lib/libvtkRenderingFreeType-7.1.so.1
#COPY --from=pdt-space-run		/usr/local/VTK-7.1.1/lib/libvtkfreetype-7.1.so.1	/usr/local/VTK-7.1.1/lib/libvtkfreetype-7.1.so.1


ENV PATH=$PATH:/usr/local/FullMonteSW/bin:/usr/bin:/usr/local/bin:/bin:/sbin:/usr/sbin:/usr/local/pdt-space/bin:/usr/local/ITKsnap/bin
ENV PATH=$PATH:/usr/local/MeshTool/bin:/usr/local/mosek-9.1.13/9.1/tools/platform/linux64x86/bin
ENV LD_LIBRARY_PATH=/usr/local/VTK-7.1.1/lib:/usr/local/MeshTool/lib:/usr/local/Qt/lib:/usr/local/pdt-space/lib:/usr/local/mosek-9.1.13/9.1/tools/platform/linux64x86/bin:/usr/local/FullMonteSW/lib:/usr/local/ITKsnap/lib
########################################################################################
